CCH Collection, by sisters Alston Daigh and Carter Johnston, offers contemporary womens clothing that is proudly produced in New York city. CCH is a collection of sophisticated basics made from fabrics sourced from the best fabric mills around the world, and sewn together in New York citys garment district. Carter designs while Alston manages production in New York. The sisters both worked at theory in New York before founding CCH Collection.

Address: 5718 Grove Avenue, Richmond, VA 23226

Phone: 804-447-8671
